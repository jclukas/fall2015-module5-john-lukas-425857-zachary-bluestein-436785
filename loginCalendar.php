<?php
// This is a *good* example of how you can implement password-based user authentication in your web application.
 
require 'database1.php';
session_start();
 
// Use a prepared statement
$stmt = $mysqli->prepare("select id, password from users where username=?");
 
// Bind the parameter
$user = $_POST['username'];
$stmt->bind_param('s', $user);

$stmt->execute();
 
// Bind the results
$stmt->bind_result($user_id, $pwd_hash);
$stmt->fetch();
 
$pwd_guess = $_POST['password'];
// Compare the submitted password to the actual password hash
if(crypt($pwd_guess, $pwd_hash)==$pwd_hash){
	// Login succeeded!
	$_SESSION['id'] = $user_id;
	// Redirect to your target page
	header ("location: view.php");
}else{
	// Login failed; redirect back to the login screen
	header ("location: homepage.php");
}
?>