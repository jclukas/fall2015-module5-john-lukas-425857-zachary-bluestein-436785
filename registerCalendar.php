<html>
<head> <title> Registration </title> 
<link rel="stylesheet" type="text/css" href="style1.css"/>

<div id="banner">

<p>Welcome to the News Site!</p>

</div>
</head>

<body>
	<div id="centering">
	<form method="POST" action="newuser.php">
		<table width="50%" border="0" cellspacing="20px" cellpadding="0" align="center">
    <tr> 
      <td>Choose a username:</td>
      <td><input type="text" name="username" placeholder="Username"></td>
    </tr>
    <tr> 
      <td>Choose a password containing at least one number:</td>
      <td><input type="text" name="password" placeholder="Password"></td>
    </tr>
    <tr> 
      <td>Create a security question:</td>
      <td><input type="text" name="question" placeholder="Security Question"></td>
    </tr>
    <tr>
    	<td>Answer to security question:</td>
    	<td><input type="text" name="answer" placeholder="Answer to Question"></td>
    </tr>
  </table>
<input type="submit" value="Create Account">
	</form>

	<form method="POST" action="homepage.php">
		<input type="submit" value="Return to Login Page">
	</form>
</div>
</body>

</html>